package com.antzuhl.zeusdemo2.service.impl;

import java.util.List;

public interface DoSomething {
    List<String> doHello();
}
