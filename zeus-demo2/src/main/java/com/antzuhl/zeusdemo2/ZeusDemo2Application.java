package com.antzuhl.zeusdemo2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZeusDemo2Application {

    public static void main(String[] args) {
        SpringApplication.run(ZeusDemo2Application.class, args);
    }

}
