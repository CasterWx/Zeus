package com.antzuhl.zeus.registry;

/**
 * ZooKeeper constant
 *
 * @author antzuhl
 */
public interface Constant {

    int ZK_SESSION_TIMEOUT = 5000;

    String ZK_NODE_DATA = "NODE";
    String ZK_REGISTRY_PATH = "/registry";

    // add NameSpace
}
